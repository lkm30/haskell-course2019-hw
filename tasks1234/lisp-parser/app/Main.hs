module Main where

import Lib
import ADT
import Parser
import Text.Megaparsec

main :: IO ()
main = do
    parseTest parseExpr "(define (factorial x) (if (= x 1) 1 (* x (factorial (- x 1)))))"
    parseTest parseExpr "`(0 ,(+ 1 2) 3)"
    parseTest parseExpr "(+ (- 1 2) -3)"


                     
