module Main where

import CInterp
import Lib

main :: IO ()
main = test

fib :: Prog
fib = Declare "fibonacci" ["n"] $
        Cond 
          ((Eq (Var "n") $ (Literal $ CInt 0)) :+: (Eq (Var "n") $ (Literal $ CInt 1)))
          (Ret (Literal $ CInt 1))
          (Ret (
                (Call "fibonacci" [Var "n" :-: (Literal $ CInt 1)]) :+: (Call "fibonacci" [Var "n" :-: (Literal $ CInt 2)])
              ) 
          )
factorial :: Prog
factorial = Declare "factorial" ["n"] $
        Cond 
          (Eq (Var "n") (Literal $ CInt 1))
          (Ret (Literal $ CInt 1))
          (Ret (
               (Var "n") :*: (Call "factorial" [((Var "n") :-: (Literal $ CInt 1))])
              )
          )