{-# OPTIONS_GHC -Wall #-}

module Main where

import Text.Megaparsec
import Parser
import Text.Pretty.Simple (pPrint)
import AST
import Interpreter
import System.IO
import qualified Data.Map as M
import Data.Function

main :: IO ()
main = do
    repl iniState where
        iniState = VM {output = "", 
                       stack = [], 
                       rstack = [],
                       dict = M.fromList []
                       }

repl :: VM -> IO ()
repl vm = do
    putStr "FORTH> "
    hFlush stdout
    input <- getLine
    case parse forth "" input of
        Left bundle -> do 
            putStrLn (errorBundlePretty bundle)
            repl vm {output = ""} --flushing output before new repl
        Right exs -> case eval vm exs of
            Left err -> do 
                putStrLn $ show err
                repl vm {output = ""}
            Right vm' -> do
                putStrLn (vm' & output)
                pPrint (vm' & dict) --showing current VM state for debugging
                pPrint (vm' & stack)
                repl vm' {output = ""}
