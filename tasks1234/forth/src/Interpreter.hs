{-# OPTIONS_GHC -Wall #-}

module Interpreter where

import AST
import Data.Function
import qualified Data.Map as M

eval :: VM -> Forth -> Either FError VM
eval vm [] = do
    return vm
eval vm (ex:exs) = helper ex where
    helper (Push x) = do
        eval vm' exs where
            vm' = vm {stack = x : (vm & stack)}

    helper (Output) = do
        case vm & stack of 
            (x:_) -> eval vm' exs where
                vm' = vm {output = (vm & output) ++ show x ++ " "}
            [] -> Left $ StackUnderflow

    helper (Def name expr) = do
        eval vm' exs where
            vm' = vm {dict = M.insert name expr (vm & dict)}

    --implemented condition and loop as desugaring
    helper (Cond ifBr elBr) = do
        case vm & stack of 
            (x:xs) -> eval vm' $ br ++ exs where
                br = if x == 0 then elBr else ifBr
                vm' = vm {stack = xs}
            _ -> Left $ StackUnderflow

    helper (Loop expr) = do
        case vm & stack of 
            (x:y:xs) -> eval vm' $ loop ++ exs where
                loop = if y > x 
                    then 
                        expr ++ [Push $ y - 1, Push x, Loop expr] 
                    else 
                        []
                vm' = vm {stack = xs}
            _ -> Left $ StackUnderflow
    
    helper (Word s) = do
        case M.lookup s (vm & dict) of
            Nothing -> Left $ LookupError s
            Just expr -> eval vm $ expr ++ exs

    helper (Prim s) = do
        case M.lookup s primDict of
            Nothing -> Left $ UndefinedPrimError s
            Just res -> case res $ vm of
                Nothing -> Left $ StackUnderflow
                Just vm' -> eval vm' exs
