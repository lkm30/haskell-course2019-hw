# kotlin
### implemented:
* first version AST (`data Expr`) and 2 supporting structs
* parsers for values (without `Array` and users' types) and parser for expressions
### planned
* parsers for initialization variable, for `if`, `for`, `while`, for functions etc.
