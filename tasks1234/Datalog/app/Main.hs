module Main where

import AST

-- son(Bill, Joey). son(Joey, Paul). son (Joey, Gleb).
-- grandfather(X, Y) :- son(Y, Z), son(Z, X).

sons :: [Statement]
sons = 
    [
        Assertion 
        (
            Clause
            {
                lit = Predicate
                {
                    name = Name "son",
                    terms = [Constant (Name "bill"), Constant (Name "joey")]
                },
                conditions = []
            }
        ),
        Assertion 
        (
            Clause
            {
                lit = Predicate
                {
                    name = Name "son",
                    terms = [Constant (Name "joey"), Constant (Name "paul")]
                },
                conditions = []
            }
        ),
        Assertion 
        (
            Clause
            {
                lit = Predicate
                {
                    name = Name "son",
                    terms = [Constant (Name "joey"), Constant (Name "gleb")]
                },
                conditions = []
            }
        ),
        Assertion 
        (
            Clause
            {
                lit = Predicate
                {
                    name = Name "grandfather",
                    terms = [Variable (Name "X"), Variable (Name "Y")]
                },
                conditions = 
                [
                    Predicate
                    {
                        name = Name "son",
                        terms = [Variable (Name "Y"), Variable (Name "Z")]
                    },
                    Predicate
                    {
                        name = Name "son",
                        terms = [Variable (Name "Z"), Variable (Name "X")]
                    }
                ]
            }
        )
    ]


main :: IO ()
main = print "Hello World"