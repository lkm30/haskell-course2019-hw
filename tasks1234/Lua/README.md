# Tasks 1, 2, 3, 4

## Already done:

#### Data types

1. No type definition before execution except constants. 

#### Parser

1. Set priorities to operations.
2. Basic string content.
3. Correct expressions without parentheses.

## Planned:

#### Data types

1. Implement hash table.
2. Create local and global scopes with tables.

#### Parser

1. Fix comments.
2. More string content.
3. Values by default in for cycle.
