module Main where

import Lib
import AST

main :: IO ()
main = print "Hello, World"

{-
factorial = Function "Factorial" ["N"] 
			If ((Val $ PyTypes $ PyNum $ Int N) 'EQ' (Val $ PyTypes $ PyNum $ Int 1)) 
				(Return (Val $ PyTypes $ PyNum $ Int N))
			(Return (Multiply (Val $ PyTypes $ PyNum $ Int N) (Function "Factorial" 
				Subtract (Val $ PyTypes $ PyNum $ Int N) (Val $ PyTypes $ PyNum $ Int 1)])) 
-}

